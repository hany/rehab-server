package com.namozag.rehab.web;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.namozag.rehab.repository.LocationRepository;
import com.namozag.rehab.repository.entity.Location;
import com.namozag.rehab.service.ApplicationService;
import com.namozag.rehab.service.model.Place;

@Controller
public class MainController {

	@Autowired LocationRepository locationRepository;
	
	@Autowired ApplicationService service;
	
	@PostConstruct
	public void init() {
//		System.out.println(":::: Locations");
//		System.out.println(locationRepository.findAll());
//		
//		System.out.println(":::: Places");
//		System.out.println(service.findAllPlaces());
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return "Hello World!";
	}
	
	@RequestMapping("/locations")
	@ResponseBody
	public List<Location> locations() {
		return locationRepository.findAll();
	}
	
	@RequestMapping("/places")
	@ResponseBody
	@Cacheable("places")
	public List<Place> places() {
		System.out.println("/places");
		return service.findAllPlaces();
	}
	
	@RequestMapping("/places/generate")
	@ResponseBody
	@CachePut("places")
	public List<Place> generatePlaces() {
//		return service.generatePlaces();
		System.out.println("/places/generate");
		return service.findAllPlaces();
	}

}
