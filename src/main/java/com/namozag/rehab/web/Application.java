package com.namozag.rehab.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.namozag.rehab.repository.entity.Location;
import com.namozag.rehab.service.ApplicationService;

@SpringBootApplication
@EnableAutoConfiguration
@EnableCaching
@Configuration
@EntityScan(basePackageClasses = Location.class)
@EnableJpaRepositories( basePackages = "com.namozag.rehab.repository" )
@ComponentScan( basePackageClasses = { MainController.class, ApplicationService.class } )
public class Application {
	
	public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        System.out.println(":::: APPLICATION STARTED");
    }

}
