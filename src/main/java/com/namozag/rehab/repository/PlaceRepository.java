package com.namozag.rehab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.namozag.rehab.repository.entity.Place;

public interface PlaceRepository extends JpaRepository<Place, Integer> {
	
}
