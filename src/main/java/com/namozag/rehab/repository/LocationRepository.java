package com.namozag.rehab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.namozag.rehab.repository.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer>{

}
