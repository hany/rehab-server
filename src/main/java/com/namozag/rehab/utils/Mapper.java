package com.namozag.rehab.utils;

import java.util.ArrayList;
import java.util.List;

public class Mapper {
	
	public static List<com.namozag.rehab.service.model.Place> mapPlaces(List<com.namozag.rehab.repository.entity.Place> entities) {
		List<com.namozag.rehab.service.model.Place> models = new ArrayList<com.namozag.rehab.service.model.Place>();
		
		for(com.namozag.rehab.repository.entity.Place entity : entities) {
			models.add(mapPlace(entity));
		}
		
		return models;
	}
	
	public static com.namozag.rehab.service.model.Place mapPlace(com.namozag.rehab.repository.entity.Place e) {
		com.namozag.rehab.service.model.Place m = new com.namozag.rehab.service.model.Place();
		
		m.setCategoryId(e.getCategoryId());
		m.setFacebook(e.getFacebook());
		m.setId(e.getId());
		m.setLocationId(e.getLocationId());
		m.setName(e.getName());
		m.setShopNumber(e.getShopNumber());
		m.setShortNo(e.getShortNo());
		m.setType(e.getType());
		m.setWebsite(e.getWebsite());
		
		if(hasValue(e.getMobile())) {
			List<String> list = new ArrayList<String>();
			m.setMobiles(list);
			list.add(e.getMobile());
			addValue(list, e.getMobile2());
			addValue(list, e.getMobile3());
			addValue(list, e.getMobile4());
		}
		
		if(hasValue(e.getPhone())) {
			List<String> list = new ArrayList<String>();
			m.setPhones(list);
			list.add(e.getPhone());
			addValue(list, e.getPhone2());
			addValue(list, e.getPhone3());
		}
		
		return m;
	}

	private static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}
	
	private static boolean hasValue(String str) {
		return str != null && str.trim().length() > 0;
	}
	
	private static void addValue(List list, String str) {
		if(hasValue(str)) {
			list.add(str.trim());
		}
	}
	
}
