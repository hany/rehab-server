package com.namozag.rehab.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.namozag.rehab.repository.PlaceRepository;
import com.namozag.rehab.service.model.Place;
import com.namozag.rehab.utils.Mapper;

@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired PlaceRepository placeRepository;
	
//	@Cacheable("places")
	public List<Place> findAllPlaces() {
		System.out.println("ApplicationServiceImpl::findAllPlaces()");
		return Mapper.mapPlaces(placeRepository.findAll());
	}

	public String generatePlaces() {
		List<Place> places = findAllPlaces();
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		try {
			return mapper.writeValueAsString(places);
		} catch (JsonProcessingException e) {
			return e.getMessage();
		}
	}

}
