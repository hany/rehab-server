package com.namozag.rehab.service;

import java.util.List;

import com.namozag.rehab.service.model.Place;

public interface ApplicationService {
	
	List<Place> findAllPlaces();

	String generatePlaces();
	
}
